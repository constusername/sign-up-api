# API Endpoint

* [Sign Up](https://signup-api.000webhostapp.com/api.php)
* [Users](https://signup-api.000webhostapp.com)

## Screenshots

### Postman 

* Posting valid data

![](img/1.png)
![](img/2.png)
![](img/3.png)  

* Posting empty data

![](img/4.png)

* Posting duplicate data

![](img/5.png)

### Database 

![](img/6.png)

### HTML

![](img/7.png)
![](img/8.png)

* The project is hosted on [https://000webhostapp.com](https://000webhostapp.com) and open-sourced on gitlab.
