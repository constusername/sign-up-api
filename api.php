<?php
// Creating connection to the database
class Database
{
    private $host = "localhost";
    private $db_name = "api";
    private $username = "root";
    private $password = "Pepcoding#1";
    public $conn;

    public function getConnection()
    {

        $this->conn = null;

        try
        {
            $this->conn = new PDO("mysql:host=" . $this->host . ";dbname=" . $this->db_name, $this->username, $this->password);
            $this
                ->conn
                ->exec("set names utf8");
        }
        catch(PDOException $exception)
        {
            echo "Connection error: " . $exception->getMessage();
        }

        return $this->conn;
    }
}

// Creating User Class
class User
{

    private $conn;
    private $table_name = "users";
    public $id;
    public $username;
    public $email;
    public $mobile;
    public $password;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function signup()
    {

        if ($this->ifPresent())
        {
            return false;
        }
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    username=:username, email=:email, mobile=:mobile, password=:password";

        $stmt = $this
            ->conn
            ->prepare($query);

        // parsing the data
        $this->username = htmlspecialchars(strip_tags($this->username));
        $this->email = htmlspecialchars(strip_tags($this->email));
        $this->mobile = htmlspecialchars(strip_tags($this->mobile));
        $this->password = htmlspecialchars(strip_tags($this->password));

        // validating the data
        if (empty($this->username) || empty($this->email) || empty($this->mobile) || empty($this->password))
        {
            return false;
        }

        $stmt->bindParam(":username", $this->username);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":mobile", $this->mobile);
        $stmt->bindParam(":password", $this->password);

        if ($stmt->execute())
        {
            $this->id = $this
                ->conn
                ->lastInsertId();
            return true;
        }

        return false;

    }

    // Checking if Username already exist
    function ifPresent()
    {
        $query = "SELECT *
            FROM
                " . $this->table_name . " 
            WHERE
                username='" . $this->username . "'";
        $stmt = $this
            ->conn
            ->prepare($query);
        $stmt->execute();
        if ($stmt->rowCount() > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}

$database = new Database();
$db = $database->getConnection();

// creating a new user object
$user = new User($db);
// fetching data
$user->username = $_POST['username'];
$user->email = $_POST['email'];
$user->mobile = $_POST['mobile'];
// encoding password with base64 encryption
$user->password = base64_encode($_POST['password']);

// populating database
if ($user->signup())
{
    // json msg on successfull sign-up
    $user_arr = array(
        "status" => true,
        "message" => "Registration successfull!",
        "username" => $user->username,
    );
}
else
{
    // json msg on failed sign-up
    $user_arr = array(
        "code" => 400, 
        "status" => false,
        "message" => "Invalid parameters or username already exists!"
    );
}
print_r(json_encode($user_arr));
?>
